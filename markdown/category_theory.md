# Category theory

This chapter contains an introduction to category theory.
Category theory is one of the preliminaries to the HoTT
book that isn't discussed in much details. In this chapter
we will try to give you a super simple foundation of it.

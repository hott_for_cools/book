# Homotopy type theory

> "We seem to get a fair number of people who hit the beginning of chapter 2
> and think "oh no, I don't know what an $\infty$-groupoid is" and get stuck "..."I
> would like the message of the book to be that you don't need to already know
> what an $\infty$-groupoid is; you can learn what one is by learning to work with
> types. But even with that goal, it seems that we have to give some intuitive
> picture of what this is supposed to mean, and we also have to make
> connections for the readers who do know what an $\infty$-groupoid is. Maybe for the
> 2nd edition we can find some better way of reconciling these goals"
> @hottissue740

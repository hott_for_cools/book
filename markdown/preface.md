# Preface

Homotopy Type Theory is an exciting and relatively new area of mathematics. The
main resource for learning it is a Free/Open Source book with the same name:
[Homotopy Type Theory: Univalent Foundations of
Mathematics](https://github.com/HoTT/book). I'm currently reading it and I
decided to publish notes that I'm taking from it. For each chapter of the
original book we have a corresponding chapter with the same name containing a
cheat sheet of theorems of that chapter and some extra notes.


## Disclaimer

* I am not a mathematician
* I am not a native English speaker

So be prepared for lots of mathematical or grammatical mistakes. However, this
is a free book, so you can help me make it better. See the [How to
Contribute](#how-to-contribute) section.

* This book is a work in progress and will change frequently:

So always check that you have the latest release.


## How to Contribute

This is a libre/open source project. It is hosted on
[GitLab](https://gitlab.com/hott_for_cools) and any contribution is welcomed
and highly appreciated.

**[TODO]** Add detailed information on how to commit to a git repository for
beginners.

### List of Contributors



## Acknowledgment

This book and most of its latex macros is based on the [Homotopy Type Theory:
Univalent Foundations of Mathematics](https://github.com/HoTT/book) book.

The PDF, web pages and e-books are produced with scripts around
[pandoc](https://pandoc.org/) and other free software. see the [GitLab
page](https://gitlab.com/hott_for_cools).


## License

Except As Otherwise Noted, This Work Is Licensed Under A Creative Commons
Attribution-ShareAlike 4.0 International License. To View A Copy Of This
License, Visit: [https://creativecommons.org/licenses/by-sa/4.0/
](https://creativecommons.org/licenses/by-sa/4.0/)

Code Samples Are Licensed Under The GNU General Public License v3.0. To View A
Copy Of This License, Visit:
[https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html)

# Type theory

## Type theory versus set theory

**Proposition:** A statement susceptible to proof.

**Theorem:** A proposition that has been proven.

**Deductive system:** A collection of rules for deriving judgments.

**Judgment (first order logic):** For the proposition $A$, "$A$ has a proof" is
a judgment.

**Judgment (type theory):** $a:A$ is the basic judgment and pronounced as "the
term $a$ has type $A$".

**Propositions are types:** Finding an element of a type is proving that
proposition. Thus we call an element of a type a *witness* or an *evidence* of
the truth of a proposition.

**Propositional equality:** When $\id[A]ab$ is inhabited, we say that $a$ and
$b$ are propositionally equal.

**Judgmental (or definitional) equality:** When two expressions are equal by
definition. You expand and compute the definitions and conclude that they are
equal. We write it as $a\jdeq b : A$ or simply $a \jdeq b$. Use $\defeq$ to
introduce definitional equality.

**Assumption:** $x:A$, where $x$ is a variable and $A$ is a type.

**Context:** The collection of all such assumptions.

**Type former:** A way to construct types, together with rules for the
construction and behavior of elements of that type.

For the rest of this chapter we will introduce these type formers:

* Function types
* Universes and families
* Dependent function types ($\Pi$-types)
* Product types
* Dependent pair types ($\Sigma$-types)
* Coproduct types
* The type of booleans
* The natural numbers
* Pattern matching and recursion
* Propositions as types
* Identity types
    * Path induction
    * Equivalence of path induction and based path induction
    * Disequality


English                                Type Theory
-----------------------------------    ----------------------------
True                                   $\unit$
False                                  $\emptyt$
$A$ and $B$                            $A \times B$
$A$ or $B$                             $A + B$
If $A$ then $B$                        $A \to B$
$A$ if and only if $B$                 $(A \to B) \times (B \to A)$
Not $A$                                $A \to \emptyt$
For all $x:A$, $P(x)$ holds            $\prd{x:A} P(x)$
There exists $x:A$ such that $P(x)$    $\sm{x:A}$ $P(x)$

How to introduce a new kind of type:

1. **Formation rules:** How to form new types of this kind.

2. **Introduction rules (type's constructors):** How to construct elements of
   that type by:
    * **Direct definition**
    * **$\lambda$-abstraction:** When we don't want to introduce a name for the
         function.

3. **Elimination rules (type's eliminators):** How to use elements of that
   type.

4. **Computation rule ($\beta$-reduction):** How an eliminator acts on a
   constructor.

5. **Uniqueness principle ($\eta$-expansion) (optional):** Expresses the
   uniqueness of maps into or out of that type.

6. **Propositional uniqueness principle (optional):** When the uniqueness
   principle is not taken as a rule of judgmental equality, we can prove it as
   a *propositional* equality from the other rules for the type.


## Function types

1. **Formation rules:** Given types $A$ and $B$, we can construct the type
   $A \to B$ of functions with domain $A$ and codomain $B$.

2. **Introduction rules:**

    * **Direct definition:** Defining $f : A \to B$ by $f(x) \defeq \Phi$
      where $x$ is a variable and $\Phi$ is an expression which may use $x$.

    * **$\lambda$-abstraction:** $$(\lamt{x:A}\Phi) : A \to B.$$

3. **Elimination rules:** Given a function $f : A \to B$
   and an element of the domain $a : A$, we can apply the function to obtain an
   element of the codomain $B$, denoted $f(a)$ and called the value of $f$ at
   $a$.

4. **Computation rule:** $(\lamu{x:A}\Phi)(a) \jdeq \Phi'$ where $\Phi'$ is the
   expression $\Phi$ in which all occurrences of $x$ have been replaced by $a$.

5. **Uniqueness principle:** $f \jdeq (\lam{x} f(x))$. It shows that $f$ is
   *uniquely* determined by its values.


## Universes and families

**Universe:** a type whose elements are types.

**Hierarchy of universes:** $\UU_0 : \UU_1 : \UU_2 : \cdots$ where every
universe $\UU_i$ is an element of the next universe. We assume that they are
cumulative: All the elements of the $i^{\mathrm{th}}$ universe are also
elements of the $(i+1)^{\mathrm{st}}$ universe.

**Typical ambiguity:** Omitting the level $i$ for convenience.

**Families of types:** Using functions $B : A \to \UU$ whose
codomain is a universe to model a collection of types varying over a given type
$A$.


## Dependent function types

1. **Formation rules:** Given a type $A:\UU$ and a family $B:A \to \UU$, we may
   construct the type of dependent functions $\prd{x:A}B(x) : \UU$.

2. **Introduction rules:**

    * **Direct definition:** To define $f : \prd{x:A}B(x)$, where $f$ is the
      name of a dependent function to be defined, we need an expression
      $\Phi : B(x)$ possibly involving the variable $x:A$, and we write
      $$f(x) \defeq \Phi \qquad \mbox{for $x:A$}.$$

    * **$\lambda$-abstraction:** $$\lamu{x:A} \Phi \ :\ \prd{x:A} B(x).$$

3. **Elimination rules:** **Applying** $f : \prd{x:A}B(x)$ to an argument $a:A$
   to obtain an element $f(a):B(a)$.

4. **Computation rule:** Given $a:A$ we have $f(a) \jdeq \Phi'$ and
   $(\lamu{x:A} \Phi)(a) \jdeq \Phi'$, where $\Phi'$ is obtained by replacing
   all occurrences of $x$ in $\Phi$ by $a$.

5. **Uniqueness principle:** $f\jdeq (\lam{x} f(x))$ for any
   $f:\prd{x:A} B(x)$.


**Polymorphic function:** A class of dependent function types which takes a
type as one of its arguments and acts on elements of that type.


## Product types

1. **Formation rules:** Given types $A,B:\UU$ we introduce the type
   $A\times B:\UU$, which we call their **cartesian product**.

2. **Introduction rules:**

    * **Direct definition:** Given $a:A$ and $b:B$, we may form $(a,b):A\times B$.

    * **$\lambda$-abstraction:**

3. **Elimination rules:**
   For any $g : A \to B \to C$,
   we can define a function $f : A\times B \to C$ by
   $$f(\tup{a}{b}) \defeq g(a)(b).$$

4. **Computation rule:**
   $$f(\tup{a}{b}) \defeq g(a)(b).$$

5. **Propositional uniqueness principle:** Every element of $A\times B$ is a
   pair.


**Unit type:**
We call the nullary product type the **unit type** $\unit : \UU$.
The only element of $\unit$ is some particular object
$\ttt : \unit$.

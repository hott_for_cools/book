# Welcome to HoTT for Cools

This book consists of notes that I'm taking from [Homotopy Type Theory:
Univalent Foundations of Mathematics](https://github.com/HoTT/book).

It is available on
[https://www.mehranbaghi.com/hott_for_cools/](https://www.mehranbaghi.com/hott_for_cools/)

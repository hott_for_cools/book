#!/bin/bash


set -e

inkscape () { /opt/squashfs-root/AppRun $@; }
ebook-convert () { /opt/calibre/ebook-convert $@; }

name="hott_for_cools"
mkdir -p ./building/svg/inline
mkdir -p ./building/svg/display

# Create svgs
svgmk () {
    while read -r filename
    do
        cat ../../../markdown/$filename.md >> $name.md
        echo -e '\n\n' >> $name.md
    done < ../../../markdown/order
    cp ../../../builder/macros.tex .
    pandoc -f markdown -t json $name.md | ../../../builder/eqn_"$1".py > /dev/null
    if [[ -f equations.txt ]]
    then
        ../../../builder/hash.sh $1
        unset line
        while read -r line
        do
            if [[ ! -f $line.svg ]]
            then
                texi2pdf $line.tex &> /dev/null
                pdf2svg $line.pdf $line.svg
                rsvg-convert $line.svg -z "1.4" -f svg -o "$line"_rsvg.svg
                rm $line.svg
                mv "$line"_rsvg.svg $line.svg
                inkscape --export-area-drawing --export-overwrite $line.svg &> /dev/null
                svgo $line.svg
                sed -i -E "s|$|\n|g" $line.svg
            fi
        done < sha256.txt
        unset line
        while read -r line
        do
            ls -a | grep $line | grep -v 'svg' | xargs /bin/rm
        done < sha256.txt
        rm equations.txt uniqequations.txt sha256.txt
    fi
}
cd ./building/svg/inline
svgmk inline
cd ../display
svgmk display

# Build Website
stylelint ../../../builder/main.css --config ../../../builder/stylelint.json
stylelint ../../../builder/toc.css --config ../../../builder/stylelint.json
postcss -c ../../../builder/postcss.config.js ../../../builder/main.css > ./main.css
postcss -c ../../../builder/postcss.config.js ../../../builder/toc.css > ./toc.css
pandoc -f markdown-auto_identifiers -t html5 --bibliography ../../../builder/references.bib --lua-filter="../../../builder/auto_identifiers_underscore.lua" --filter pandoc-citeproc --csl ../../../builder/chicago-fullnote-bibliography.csl --filter ../../../builder/macrohash.py $name.md -o $name-hash.html
cat ../../../builder/machead ./$name-hash.html > ./$name-hash-mac.html
m4 $name-hash-mac.html > $name-prenum.html
hxnum $name-prenum.html > $name-pretoc.html
hxmultitoc $name-pretoc.html > toc.html
i=1
while read -r chapter
do
    pandoc -f markdown-auto_identifiers -t html5 --bibliography ../../../builder/references.bib --lua-filter="../../../builder/auto_identifiers_underscore.lua" --filter pandoc-citeproc --csl ../../../builder/chicago-fullnote-bibliography.csl -M suppress-bibliography=true --filter ../../../builder/macrohash.py ../../../markdown/$chapter.md -o $chapter-hash.html
    cat ../../../builder/machead ./$chapter-hash.html > ./$chapter-hash-mac.html
    m4 $chapter-hash-mac.html > $chapter-prenum.html
    echo $chapter > chapt
    hxnum $chapter-prenum.html > $chapter-numed.html
    sed -E -i "s|<span\ class=\"secno\">[0-9]+\.|<span\ class=\"secno\">"$i"\.|g" $chapter-numed.html
    ((i++))
    if [[ $chapter == "bibliography" ]]
    then
        cat ./$name-hash.html | hxselect "#refs.references" | cat ../../../builder/header.html $chapter-numed.html - ../../../builder/footer.html | m4 > $chapter"-premini.html"
    else
        cat ../../../builder/header.html $chapter-numed.html ../../../builder/footer.html | m4 > $chapter"-premini.html"
    fi
    html-minifier -c ../../../builder/html-minifier.conf $chapter"-premini.html" | sed -E "s|<hr>|\<h2\>Footnotes\ and\ References\<\/h2\>|g" > $chapter
    rm $chapter-hash.html $chapter-hash-mac.html $chapter-prenum.html $chapter-premini.html $chapter-numed.html
done < ../../../markdown/order
unset i
../../../builder/toc_link_fixer.sh toc.html
m4 ../../../builder/Table_of_Content_PreM4.html > $name-premini.html
html-minifier -c ../../../builder/html-minifier.conf $name-premini.html > index.html

# Build PDF
pandoc -N --template=../../../builder/template.tex --variable header-includes="\input{macros}" --variable documentclass="book" --variable mathspec --variable mainfont="TeX Gyre Pagella" --variable sansfont="FreeSans" --variable monofont="Liberation Mono" --variable fontsize="11pt, oneside" --variable colorlinks -M date="$(date "+%B %e, %Y")" --variable version=2.0 $name.md --toc --bibliography ../../../builder/references.bib --filter pandoc-citeproc --csl ../../../builder/chicago-fullnote-bibliography.csl -o $name.pdf

# Build EPUB
pandoc -f markdown -t markdown --filter ../../../builder/markdownm4-epub.py $name.md -o $name-include-svg.md
pandoc -N -o $name-pre-m4.epub --bibliography ../../../builder/references.bib --filter pandoc-citeproc --csl ../../../builder/chicago-fullnote-bibliography.csl ../../../builder/title.txt $name-include-svg.md --epub-cover-image=../../../builder/HoTT_Doughnut_Coffee.svg --css=main.css
unzip -d $name-extracted $name-pre-m4.epub
cd $name-extracted/EPUB/text/
for chapter in $(ls -1 | grep -E "^ch.*\.xhtml$")
do
    cat ../../../../../../builder/machead $chapter > $chapter-pre-m4.xhtml
    m4 $chapter-pre-m4.xhtml > $chapter
    rm $chapter-pre-m4.xhtml
done
cd ../../
zip -r zipped-file .
mv zipped-file.zip ../$name.epub
cd ..
rm -rf ./$name-extracted

# Build MOBI
ebook-convert $name.epub $name.mobi --dont-compress --mobi-file-type both --mobi-keep-original-images --cover "../../../builder/HoTT_Doughnut_Coffee.svg"

# Cleanup
rm toc.html $name-hash.html $name-hash-mac.html $name-premini.html main.css toc.css chapt
rm macros.tex $name.md $name-include-svg.md $name-pre-m4.epub $name-prenum.html $name-pretoc.html
mkdir ../artifacts
ls -1 | grep -E -v "^.*\.svg$" | xargs -I '{}' mv '{}' ../artifacts
mv ../artifacts ../..
clsvg () {
    if [[ -n $(ls -1 | grep -E -v "^.*\.svg$") ]]
    then
        ls -1 | grep -E -v "^.*\.svg$" | xargs rm
    fi
}
clsvg
cd ../inline && clsvg

# Deploy
cd ../..
mkdir mehranbaghicom && cd mehranbaghicom
wget https://gitlab.com/libre_vad/book/-/jobs/artifacts/master/download?job=website
mv download\?job\=website libre_vad.zip
unzip libre_vad.zip
rm libre_vad.zip
mkdir libre_vad
mv building/artifacts/* libre_vad
rm -rf building
wget https://gitlab.com/baghi/website/-/jobs/artifacts/master/download?job=website
mv download\?job\=website main.zip
unzip main.zip
rm main.zip
mv building/* .
rm -rf building
mkdir hott_for_cools
cp ../artifacts/* hott_for_cools
cd ..
zip -r website.zip mehranbaghicom
curl -H "Content-Type: application/zip" -H "Authorization: Bearer ${NETLIFY_TOKEN}" --data-binary "@website.zip" https://api.netlify.com/api/v1/sites/${NETLIFY_ID}/deploys > /dev/null 2>&1

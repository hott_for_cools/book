#!/usr/bin/env python

from pandocfilters import toJSONFilter


def extract(key, value, format, meta):

    if key == 'Math':
        if value[0][u't'] == "InlineMath":
            f = open("equations.txt", "a")
            f.write(value[1] + "\n")
            f.close()


if __name__ == "__main__":
    toJSONFilter(extract)

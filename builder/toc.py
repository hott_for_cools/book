#!/usr/bin/env python


from pandocfilters import toJSONFilter


def extract(key, value, format, meta):

    if key == 'html':
        return value[0]


if __name__ == "__main__":
    toJSONFilter(extract)

#!/bin/bash


set -e
cat equations.txt | sort -u > ./uniqequations.txt

unset line
while read -r line
do
    echo -n $line | sha256sum | cut -d' ' -f1 >> sha256.txt
done < ./uniqequations.txt

unset line
while read -r line
do
    cp ../../../builder/header.tex $line.tex
done < sha256.txt

unset line
while read -r line
do
    hash=$(echo -n $line | sha256sum | cut -d' ' -f1)
    if [[ $1 == "display" ]]
    then
        echo "\[" >> $hash.tex
    else
        echo "\(" >> $hash.tex
    fi
    echo $line >> $hash.tex
    if [[ $1 == "display" ]]
    then
        echo "\]" >> $hash.tex
    else
        echo "\)" >> $hash.tex
    fi
    cat ../../../builder/footer.tex >> $hash.tex
done < ./uniqequations.txt
unset line
